import Vue from 'vue';
import App from './App.vue';
import router from './router';
import store from './store';

Vue.config.productionTip = false;

const app = new Vue({
  router,
  store,
  render: (h) => h(App),
}).$mount('#app');

window.addEventListener('beforeunload', () => {
  const notesString = JSON.stringify(app.$store.state.notes);
  localStorage.setItem('notes', notesString);
});

document.addEventListener('DOMContentLoaded', () => {
  const notesString = localStorage.getItem('notes');
  const notesJSON = JSON.parse(notesString);
  app.$store.state.notes = notesJSON === null ? [] : notesJSON;
});
