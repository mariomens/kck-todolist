import Vue from 'vue';
import VueRouter from 'vue-router';
import Home from '../views/Home.vue';

Vue.use(VueRouter);

const routes = [
  {
    path: '/',
    name: 'Home',
    component: Home,
  },
  {
    path: '/table',
    name: 'Table',
    component: () => import(/* webpackChunkName: "about" */ '../views/Table.vue'),
  },
  {
    path: '/tableNormal',
    name: 'TableNormal',
    component: () => import(/* webpackChunkName: "about" */ '../views/TableNormal.vue'),
  },
];

const router = new VueRouter({
  routes,
});

export default router;
